#!/bin/bash

javac -classpath jooq-3.11.0.jar:jooq-meta-3.11.0.jar:jooq-codegen-3.11.0.jar:mysql-connector-java-5.1.46-bin.jar:. -d ./ ../business/src/main/java/mathias/xprssdlvry/restapi/JooqGeneratorStrategy.java

java -classpath jooq-3.11.0.jar:jooq-meta-3.11.0.jar:jooq-codegen-3.11.0.jar:mysql-connector-java-5.1.46-bin.jar:. org.jooq.codegen.GenerationTool xprssdlvry.xml