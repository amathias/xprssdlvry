package mathias.xprssdlvry.restapi;

import mathias.xprssdlvry.restapi.security.JWTAuthenticationFilter;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import spark.Spark;

@Configuration
@ComponentScan({"mathias.xprssdlvry.restapi"})
public class RestApiApplication {

    public static void main(String ... args ) {

        Spark.port(8080);

        new AnnotationConfigApplicationContext(RestApiApplication.class);

        Spark.before(Path.API, JWTAuthenticationFilter.checkAuthentication);

        Spark.after(((request, response) -> response.type("application/json")));

        Spark.exception(UsernameNotFoundException.class, (e, request, response) -> {
            e.printStackTrace();
            response.status(401);
            response.body(e.getMessage());
        });

        Spark.exception(RuntimeException.class, (e, request, response) -> {
            e.printStackTrace();
            response.status(500);
            response.body("Unexpected error!");
        });

        Spark.get("*", (request, response) -> {
            response.status(404);
            return "";
        });


    }

    private static class Path {
        private static final String API = "/api/*";
    }

}
