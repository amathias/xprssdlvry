package mathias.xprssdlvry.restapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import mathias.xprssdlvry.restapi.model.dto.UserDTO;
import mathias.xprssdlvry.restapi.security.TokenAuthenticationService;
import mathias.xprssdlvry.restapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import spark.Route;
import spark.Spark;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    private Route login = ( request, response ) -> {


        final UserDTO user = new ObjectMapper().readValue(request.raw().getInputStream(), UserDTO.class);

        final UserDetails authUser = userService.loadUserByUsername(user.getUsername());

        if (!BCrypt.checkpw(user.getPassword(), authUser.getPassword())) {
            Spark.halt(401, "Bad credentials!");
            return null;
        }

        final Authentication auth = new UsernamePasswordAuthenticationToken(
                authUser.getUsername(),
                authUser.getPassword(),
                Collections.emptyList()
        );

        TokenAuthenticationService.addAuthentication(response.raw(), user.getUsername());

        return "";
    };
    private Route newUser = (request, response) -> {

        UserDTO user = new ObjectMapper().readValue(request.raw().getInputStream(), UserDTO.class);

        user = userService.createUser(user);

        response.status(200);

        return new ObjectMapper().writeValueAsString(user);
    };
    private Route getUser = ( request, response ) -> {

        String json = new ObjectMapper().writeValueAsString(userService.findAll());

        return json;
    };

//    private Route logout = (request, response) -> {
//
//        final Authentication auth = TokenAuthenticationService.getAuthentication( request.raw() );
//
//        final Optional<UserDTO> user = userService.findByUsername( auth.getName() );
//
//        return null;
//    };

    @PostConstruct
    public void init() {

        Spark.post(Path.AUTH, login);
//        Spark.delete( Path.AUTH, logout );

        Spark.get(Path.USER, getUser);
        Spark.get(Path.USER, newUser);
    }

    private static class Path {

        private static String AUTH = "/auth";
        private static String USER = "/api/user";

    }



}
