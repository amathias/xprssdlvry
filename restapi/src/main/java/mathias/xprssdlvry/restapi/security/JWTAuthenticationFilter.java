package mathias.xprssdlvry.restapi.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import spark.Filter;
import spark.Spark;

public class JWTAuthenticationFilter {

    private static String AUTHENTICATION_SESSION_ATTRIBBUTE = "authentication";

    public static Filter checkAuthentication = (request, response) -> {

        request.session().removeAttribute(AUTHENTICATION_SESSION_ATTRIBBUTE);

        final String header = request.raw().getHeader(TokenAuthenticationService.HEADER_STRING);

        if ( header == null || ! header.startsWith( TokenAuthenticationService.TOKEN_PREFIX ) ) {

            Spark.halt(403);
            return;
        }

        final Authentication authentication = TokenAuthenticationService.getAuthentication(request.raw());

        request.session().attribute(AUTHENTICATION_SESSION_ATTRIBBUTE, authentication);

        SecurityContextHolder.getContext().setAuthentication(authentication);

    };


}
