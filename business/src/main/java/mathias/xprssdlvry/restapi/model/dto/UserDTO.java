package mathias.xprssdlvry.restapi.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class UserDTO extends BaseDTO implements Serializable {

    private String username;
    private String name;
    private LocalDateTime userSince;
    private String lastValidToken;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String password;

    public UserDTO(Long id, String username, String name, String password, LocalDateTime userSince, String lastValidToken) {
        this.id = id;
        this.username = username;
        this.name = name;
        this.password = password;
        this.userSince = userSince;
        this.lastValidToken = lastValidToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getUserSince() {
        return userSince;
    }

    public void setUserSince(LocalDateTime userSince) {
        this.userSince = userSince;
    }

    public String getLastValidToken() {
        return lastValidToken;
    }

    public void setLastValidToken(String lastValidToken) {
        this.lastValidToken = lastValidToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(username, userDTO.username);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), username);
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", userSince=" + userSince +
                ", lastValidToken='" + lastValidToken + '\'' +
                '}';
    }
}