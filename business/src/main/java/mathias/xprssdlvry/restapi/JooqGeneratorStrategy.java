package mathias.xprssdlvry.restapi;

import org.jooq.codegen.DefaultGeneratorStrategy;
import org.jooq.meta.Definition;
import org.jooq.meta.SchemaDefinition;

public class JooqGeneratorStrategy extends DefaultGeneratorStrategy {

    /**
     * Override this method to define how your Java classes and Java files should
     * be named. This example applies no custom setting and uses CamelCase versions
     * instead
     */
    @Override
    public String getJavaClassName(Definition definition, Mode mode) {

        String javaClassName = super.getJavaClassName(definition, mode);

        if (mode == Mode.DEFAULT && !(definition instanceof SchemaDefinition)) {
            javaClassName += "Table";
        }

        return javaClassName;
    }

    /**
     * Override this method to re-define the package names of your generated
     * artefacts.
     */
    @Override
    public String getJavaPackageName(Definition definition, Mode mode) {

        String packageName = super.getJavaPackageName(definition, mode);

        if (packageName.endsWith(".jooq")) {
            packageName += ".definitions";
        }

        if (packageName.contains(".tables") && !packageName.endsWith(".tables")) {
            packageName = packageName.replace(".tables", "");
        }

        return packageName;
    }


}
