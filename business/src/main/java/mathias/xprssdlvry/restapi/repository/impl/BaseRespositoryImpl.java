package mathias.xprssdlvry.restapi.repository.impl;

import mathias.xprssdlvry.restapi.model.dto.BaseDTO;
import mathias.xprssdlvry.restapi.repository.BaseRespository;
import org.jooq.DSLContext;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UpdatableRecord;
import org.jooq.impl.TableImpl;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;

public abstract class BaseRespositoryImpl<T extends Table, R extends BaseDTO> implements BaseRespository<R> {

    @Autowired
    private DSLContext context;

    private TableImpl<UpdatableRecord> table;
    private Class<R> baseClass;

    protected BaseRespositoryImpl(T table, Class<R> baseClass) {
        this.table = (TableImpl<UpdatableRecord>) table;
        this.baseClass = baseClass;
    }

    DSLContext getContext() {
        return context;
    }

    public List<R> findAll() {

        return context.select().from(table).fetchInto(baseClass);
    }

    public <ID extends Serializable> List<R> findBy(TableField field, ID value) {

        return context.select().from(table).where(field.eq(value)).fetchInto(baseClass);
    }

    public R save( R record ) {

        final UpdatableRecord _result = context.newRecord(table, record);

        _result.store();

        return record;
    }

    public void delete( R record ) {

        context.delete(table).where(table.getIdentity().getField().getName() + " = " + record.getId());
    }

}
