package mathias.xprssdlvry.restapi.repository;

import org.jooq.TableField;

import java.io.Serializable;
import java.util.List;

public interface BaseRespository<R extends Serializable> {

    List<R> findAll();

    <ID extends Serializable> List<R> findBy(TableField field, ID value);
    R save( R record );
    void delete( R record );
}
