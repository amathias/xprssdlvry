package mathias.xprssdlvry.restapi.repository.impl;

import mathias.xprssdlvry.restapi.model.dto.UserDTO;
import mathias.xprssdlvry.restapi.model.jooq.definitions.Tables;
import mathias.xprssdlvry.restapi.model.jooq.tables.UserTable;
import mathias.xprssdlvry.restapi.repository.UserRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl extends BaseRespositoryImpl<UserTable, UserDTO> implements UserRepository {

    UserRepositoryImpl() {
        super(Tables.USER, UserDTO.class);
    }

}
