package mathias.xprssdlvry.restapi.repository;

import mathias.xprssdlvry.restapi.model.dto.UserDTO;

public interface UserRepository extends BaseRespository<UserDTO> {
}
