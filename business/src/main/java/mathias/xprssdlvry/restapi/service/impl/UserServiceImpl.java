package mathias.xprssdlvry.restapi.service.impl;

import mathias.xprssdlvry.restapi.model.dto.UserDTO;
import mathias.xprssdlvry.restapi.model.jooq.definitions.Tables;
import mathias.xprssdlvry.restapi.repository.UserRepository;
import mathias.xprssdlvry.restapi.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    public Collection<UserDTO> findAll() {
        return userRepository.findAll();
    }

    public UserDetails loadUserByUsername( String username ) throws UsernameNotFoundException {

        final Optional<UserDTO> user =
                this.userRepository.findBy(Tables.USER.USERNAME, username)
                        .stream().findAny();

        if ( user.isPresent() ) {

            return org.springframework.security.core.userdetails.User.withUsername( user.get().getUsername() )
                    .password( user.get().getPassword() )
                    .roles("USER")
                    .build();

        }

        throw new UsernameNotFoundException("Usuário não encontrado");

    }

    @Override
    public Optional<UserDTO> findByUsername(String username) {
        return userRepository.findBy(Tables.USER.USERNAME, username).stream().findAny();
    }

    public UserDTO createUser(UserDTO userDTO) {

        // TODO: validate

        userDTO.setUserSince(LocalDateTime.now());
        userDTO.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt()));

        userDTO = userRepository.save(userDTO);

        return userDTO;
    }

}
