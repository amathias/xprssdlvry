package mathias.xprssdlvry.restapi.service;

import mathias.xprssdlvry.restapi.model.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collection;
import java.util.Optional;

public interface UserService {

    Collection<UserDTO> findAll();

    UserDetails loadUserByUsername(String username) throws UsernameNotFoundException;

    Optional<UserDTO> findByUsername(String username);

    UserDTO createUser(UserDTO userDTO);
}
